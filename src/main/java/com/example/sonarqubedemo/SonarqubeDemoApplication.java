package com.example.sonarqubedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.lang.Nullable;

@SpringBootApplication
public class SonarqubeDemoApplication {
    private int a=5;
    private int b=3;
    private int c=5;
    public static void main(String[] args) {
        SpringApplication.run(SonarqubeDemoApplication.class, args);
    }


    public static void a(@Nullable Object o){
        System.out.println(o.toString());
    }

    public static void a1(@Nullable Object o){
        System.out.println(o.toString());
    }

    public static void a2(@Nullable Object o){
        System.out.println(o.toString());
    }
}
